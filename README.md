# Loihi-SOEL-Gesture

This is the SOEL codebase that I wrote about in the submission to JETCAS 2020.

The training folder contains a jupyter notebook called gesture-SOEL-6.ipynb that contains an example implementation using a small one shot learning example dataset.

BEFORE running the gesture-SOEL-6.ipynb code, please look at /src/slayer2loihi.py and change variable snipDirAbsolute to where it is in your directory.

Tested with Ubuntu 16.04, nxsdk 0.95, Intel KapohoBay and Intel INRC cloud servers