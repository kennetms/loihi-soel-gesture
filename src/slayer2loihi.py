# Handles translation of SLAYER model architecture to Loihi model architecture
#
# Copyright: Kenneth Stewart, Sumit Bam Shrestha, Garrick Orchard
# GPLv3

import numpy as np
import nxsdk.api.n2a as nx
#from nxsdk.graph.nxboard import N2Board # for 0.95 with KB
from nxsdk.graph.monitor.probes import *
import scipy.sparse as sps
import pickle
import os
import inspect
import errno

import random
        
class Slayer2Loihi():
    
    @staticmethod
    def distributeCompartments(layer, corenum, compartmentsPerCore):
        """Distributes compartments across cores, starting on a new core

        param CompartmentGroup layer: The group of compartments to distribute
        param int corenum: The last used logicalCoreId
        param int compartmentsPerCore: The maximum number of compartments per core
        """
        
        corenum = np.ceil(corenum)
        for comp in layer:
            comp.logicalCoreId = int(np.floor(corenum))
            corenum = corenum+1/compartmentsPerCore

        return corenum
    
    @staticmethod
    def reorderLayer(layerIn):
        """
        Converts a compartment group from WHC to CHW order.
        
        :param CompartmentGroup layerIn: The layer to reorder.
        :return CompartmentGroup layerOut: The re-ordered layer.
        """
        
        net = layerIn.net

        layerOut = net.createCompartmentGroup()
        layerOut.sizeX = layerIn.sizeX
        layerOut.sizeY = layerIn.sizeY
        layerOut.sizeC = layerIn.sizeC

        for cc in range(layerIn.sizeC):
            for yy in range(layerIn.sizeY):
                for xx in range(layerIn.sizeX):
                    layerOut.addCompartments(layerIn[xx*layerIn.sizeY*layerIn.sizeC + yy*layerIn.sizeC + cc])

        return layerOut

    @classmethod
    def inputLayer(cls, net, inputSpec, corenum, compartmentsPerCore):
        """Create a new input layer

        param dict inputSpec: Specifies the dimensions of the layer, "sizeX", "sizeY", "sizeC"
        param int compartmentsPerCore: The maximum number of compartments per core
        """
        
        sizeX = inputSpec["sizeX"]
        sizeY = inputSpec["sizeY"]
        sizeC = inputSpec["sizeC"]
        protoInput = inputSpec["sizeC"]
        connProto = inputSpec["connProto"]
        compProto = inputSpec["compProto"]

        # create the input layer
        layerInput = net.createCompartmentGroup(size=sizeX*sizeY*sizeC, prototype=compProto)

        # add properties used by later convolution
        layerInput.sizeX = sizeX
        layerInput.sizeY = sizeY
        layerInput.sizeC = sizeC

        corenum = cls.distributeCompartments(layerInput, corenum, compartmentsPerCore)

        # create a dummy input connection. This creates the input axons our snips will send spikes to
        inStubGroup = net.createInputStubGroup(size = layerInput.numNodes)

        inputConnectionGroup = inStubGroup.connect(layerInput,
                                                   prototype = connProto,
                                                   connectionMask = sps.identity(layerInput.numNodes))

        return layerInput, inputConnectionGroup, corenum

    @staticmethod
    def writeHeader(inputConnectionGroup, layerOutput, spikesPerPacket, sampleLength, testing=0):
        """Writes the temporary header files which defines constants used by snips.
        """
        
        extraHeaderFilePath = snipDir + '/array_sizes.h'
        f = open(extraHeaderFilePath, "w")
        f.write('/* Temporary generated file for define the size of arrays before compilation */\n')
        f.write('#define num_addresses ' + str(inputConnectionGroup.numNodes)+'\n')
        f.write('#define spikes_per_packet ' + str(spikesPerPacket)+'\n')
        f.write('#define timesteps_per_sample ' + str(sampleLength)+'\n')
        f.write('#define num_classes ' + str(layerOutput.numNodes)+'\n')
        f.write('#define testing ' + str(testing)+'\n')
        

        f.close()
        
    @classmethod
    def prepSpikeInjection(cls, inputConnectionGroup, board, spikesPerPacket, sampleLength, snips, regenerateCoreAxon):
        """Determines the core/axon location of the model input axons and sets up snips
        which will later be used to inject spikes

        :param ConnectionGroup inputConnectionGroup: The input connection group for the model
        :param N2Board board: The compiled board object
        :param int spikesPerPacket: The number of spikes to send per packet
        :param int sampleLength: The number of timesteps per sample
        :param int numSnips: The number of Lakemonts to distribute spikes across
        :param bool regenerateCoreAxon: Whether to load core/axon values from file or precompute them
        """
        

        net = inputConnectionGroup.net
        numSnips = len(snips)
        # This is incredibly slow. Save the result and load from file
        if regenerateCoreAxon is True:
            # Determine the core/axon addresses
            #chip = [int]*inputConnectionGroup.numNodes
            core = [int]*inputConnectionGroup.numNodes
            axon = [int]*inputConnectionGroup.numNodes
            for ii, conn in enumerate(inputConnectionGroup):
                (_, tempChip, tempCore, axon[ii]) = net.resourceMap.inputAxon(conn.inputAxon.nodeId)[0]
                core[ii] = board.n2Chips[tempChip].n2Cores[tempCore].id
            np.savetxt(tempDir+'/axon.txt', axon, fmt='%i')
            np.savetxt(tempDir+'/core.txt', core, fmt='%i')
        else:
            axon = np.loadtxt(tempDir+'/axon.txt').reshape(inputConnectionGroup.numNodes, )
            core = np.loadtxt(tempDir+'/core.txt').reshape(inputConnectionGroup.numNodes, )

        # Setup Spike Injection Snips
        spikeSnips = [None]*numSnips
        spikeChannels = [None]*numSnips

        for idx, ii in enumerate(snips):
            spikeSnips[idx] = board.createProcess(name="runSpikes"+str(ii),
                                                 includeDir=snipDirAbsolute,
                                                 cFilePath=snipDirAbsolute + "/myspiking"+str(ii)+".c",
                                                 funcName="run_spiking"+str(ii),
                                                 guardName="do_spiking"+str(ii),
                                                 phase="spiking",
                                                 lmtId=ii)

            spikeChannels[idx] = board.createChannel(('spikeAddresses'+str(ii)).encode(), "packed", spikesPerPacket*4)
            spikeChannels[idx].connect(None, spikeSnips[idx])

        return spikeChannels, core, axon
    
    @staticmethod
    def prepSpikeData(core, axon, spikesPerPacket, layerInput, dataset, numSamples, sampleLength, snips, randList=[]):
        """
        TODO: limit numSnips to [0,1] for data, and [2] for label
        """
        numSnips = len(snips)
        sizeXin = layerInput.sizeX
        sizeYin = layerInput.sizeY
        sizeCin = layerInput.sizeC


        addresses = []
        timestamps = []
        
        
        for ii in range(numSamples):
            # Some code here to load x,y,p,ts
            # convert ts to milliseconds
            if randList:
                sample, _ = dataset[randList[ii]] #ii]
            else:
                sample, _ = dataset[ii]
            #add some blank time between samples
            ts = sample.t.astype(int) + ii*(sampleLength)
            x = sample.x
            y = sample.y
            p = sample.p

            addresses.extend((x*sizeYin*sizeCin + y*sizeCin + p).tolist())
            timestamps.extend((ts).tolist())

        #endTime = startTime + numSteps
        #print('loading {:d} spikes took {:.2f} seconds'.format(len(timestamps),(time.time()-tStart)))

        numSteps = np.max(timestamps)+1000

        timestamps = np.array(timestamps)
        addresses = np.array(addresses)

        ts = [None]*numSnips
        add = [None]*numSnips
        snipdata = [None]*numSnips
        for ii,s in enumerate(snips):
            ts[ii] = timestamps[ii::numSnips]
            add[ii] = addresses[ii::numSnips]

            timeReserved = int(3<<13) #special value for timer advance

            # combine timestamps and addresses into a single stream
            #round up to a packet + 1 packet
            numEvents = len(add[ii]) + np.max(ts[ii])
            numEvents = int(spikesPerPacket * np.ceil(1+numEvents/spikesPerPacket))

            snipdata[ii] = np.ones((numEvents*2,), dtype=int)*timeReserved
            evTime = 0
            snipIndex = 0
            for jj, loihiTime in enumerate(ts[ii]):
                while(evTime<loihiTime):
                    snipIndex+=2
                    evTime = evTime+1
                snipdata[ii][snipIndex] = core[add[ii][jj]] 
                snipIndex+=1
                snipdata[ii][snipIndex] = axon[add[ii][jj]]
                snipIndex+=1

            snipdata[ii] = np.left_shift(snipdata[ii][1::2], 16) + np.bitwise_and(snipdata[ii][0::2], (2**16)-1)

        return snipdata, numSteps
    
    @staticmethod
    def sendSpikeData(snipData, spikeChannels, spikesPerPacket):
        """Sends spike data over spikeChannels to the model during runtime. 
        
        :param list(int nparray) snipData: Data to send to snips. One nparray per snip.
        :param list(channel) spikeChannels: A list of channels to send the data over
        :param int spikePerPacket: The number of spikes to send per call to channel.write()
        """
        # send spikes here
        wordsPerPacket = int(spikesPerPacket)
        packedPerPacket = int(wordsPerPacket/16)

        index = 0
        packetNum = 0
        #tStart = time.time()
        while index != len(snipData[0]):
            for ii in range(len(spikeChannels)):
                spikeChannels[ii].write(packedPerPacket, snipData[ii][index:(index+wordsPerPacket)])
            index = index + wordsPerPacket
            packetNum = packetNum + len(spikeChannels)
            
    @staticmethod
    def sendSnipInitialization(initChannels, core, axon):
        """Sends the core/axon addresses of input axons to the snips for use as a LUT

        :param list(Channel) initChannels: The initialization channel for each lakemont
        :param list(int) core: The core address for each address
        :param list(int) axon: The axons address for each address
        """
        # send snip initialization data
        for ii in range(len(core)):
            for channel in initChannels:
                #chip is ignored. For now we assume all input compartments lie on the same chip as the snips
                channel.write(3, [0, core[ii], axon[ii]])   

    @classmethod
    def poolingLayer(cls, layerInput, poolSpec, corenum, compartmentsPerCore):
        """Create a new pooling layer. Assumes that pooling and stride are equal 
        (i.e. non-overlapping pooling regions)

        :param CompartmentGroup layerInput: The input to the pooling layer
        :param dict poolSpec: Specifies the "stride", "connProto", "compProto"
        :param int corenum: The last output of distributeCompartments()
        :param int compartmentsPerCore: The maximum number of compartments per core
        """
        # properties of the input layer
        sizeXin = layerInput.sizeX
        sizeYin = layerInput.sizeY
        sizeCin = layerInput.sizeC
        nInput = sizeCin * sizeYin * sizeXin 
        net = layerInput.net

        # properties of the pooling function
        stride = poolSpec["stride"]
        connProto = poolSpec["connProto"]
        compProto = poolSpec["compProto"]

        # properties of the output layer
        sizeXout = int(np.ceil(sizeXin/stride))
        sizeYout = int(np.ceil(sizeYin/stride))
        sizeCout = sizeCin
        nOutput = sizeXout * sizeYout * sizeCout
        layerOutput = net.createCompartmentGroup(size=nOutput, prototype=compProto)
        layerOutput.sizeX = sizeXout
        layerOutput.sizeY = sizeYout
        layerOutput.sizeC = sizeCout

        # indices into the input layer
        x = np.arange(sizeXin, dtype=int)
        y = np.arange(sizeYin, dtype=int)
        c = np.arange(sizeCin, dtype=int)

        xx, yy, cc = np.meshgrid(x, y, c)

        # calculate the source addresses as a linear index
        src = (xx*sizeCin*sizeYin + yy*sizeCin + cc).flat

        # calculate the destination addresses as a linear index
        dst = (np.floor(xx/stride)*sizeCout*sizeYout + np.floor(yy/stride)*sizeCout + cc).astype(int).flat

        connMask = sps.coo_matrix((np.ones(nInput,),
                                   (dst,src)), 
                                  shape=(nOutput,nInput))

        layerInput.connect(layerOutput, prototype=connProto, connectionMask=connMask)

        corenum = cls.distributeCompartments(layerOutput, corenum, compartmentsPerCore)

        return layerOutput, corenum

    @classmethod
    def convLayer(cls, layerInput, convSpec, corenum, compartmentsPerCore):
        """Create a new convolution layer. Assumes zero padding for 'same' convolution.
        Does not yet support stride.

        :param CompartmentGroup layerInput: The input to the convolution layer
        :param dict convSpec: Specifies "dimX","dimY","dimC" of the filter, 
                             "connProto", "compProto" prototypes of the layer,
                             "weightFile" where the weights can be read in from.
        :param int corenum: The last output of distributeCompartments()
        :param int compartmentsPerCore: The maximum number of compartments per core
        """
        # properties of the input layer
        sizeXin = layerInput.sizeX
        sizeYin = layerInput.sizeY
        sizeCin = layerInput.sizeC
        nInput = sizeXin* sizeYin * sizeCin
        net = layerInput.net

        # properties of the convolution function
        #stride = convSpec["stride"] #not implemented yet
        connProto = convSpec["connProto"]
        compProto = convSpec["compProto"]
        convX = convSpec["dimX"]
        convY = convSpec["dimY"]
        convC = convSpec["dimC"]
        filename = convSpec["weightFile"]
        #W = np.loadtxt(filename).reshape(convC, sizeCin, convY, convX)
        
        if filename is not None:
            if filename[-3:] == 'npy':
                W = np.load(filename) # pickled
            else:
                W = np.loadtxt(filename).reshape(convC, sizeCin, convY, convX) # not pickled

        # properties of the output layer
        sizeXout = sizeXin # - np.floor(convX/2)
        sizeYout = sizeYin # - np.floor(convY/2)
        sizeCout = convC
        nOutput = sizeCout * sizeYout * sizeXout
        layerOutput = net.createCompartmentGroup(size=nOutput, prototype=compProto)
        layerOutput.sizeX = sizeXout
        layerOutput.sizeY = sizeYout
        layerOutput.sizeC = sizeCout

        ## If not using zero padding
        xDst = np.arange(sizeXout)
        yDst = np.arange(sizeYout)

        xxDst, yyDst = np.meshgrid(xDst, yDst)

        # destination compartment address
        for dx in range(convX):
            xSrc = (xxDst + dx - np.floor(convX/2))
            for dy in range(convY):
                ySrc = (yyDst + dy - np.floor(convY/2))
                valid = np.logical_and(np.logical_and(np.less(xSrc,sizeXin), 
                                                      np.greater_equal(xSrc,0)),
                                       np.logical_and(np.less(ySrc,sizeYin), 
                                                      np.greater_equal(ySrc,0)))
                for cDst in np.arange(sizeCout):
                    #dst = (cDst*sizeYout*sizeXout + yyDst*sizeXout + xxDst)
                    dst = xxDst*sizeYout*sizeCout + yyDst*sizeCout + cDst 
                    dst = dst[valid].flat
                    for cSrc in np.arange(sizeCin):
                        weight = W[cDst, cSrc, dy, dx]
                        #until we're smarter about weight sharing, let's just omit
                        #any zero valued weights and see how much we can compact the model
                        if weight != 0:
                            connProto.weight = weight
                            src = xSrc*sizeYin*sizeCin + ySrc*sizeCin + cSrc 
                            src = src[valid].flat
                            connMask = sps.coo_matrix((np.ones(len(src),),
                                                       (dst,src)), 
                                                      shape=(nOutput,nInput))
                            layerInput.connect(layerOutput,
                                               prototype=connProto,
                                               connectionMask=connMask)

        corenum = cls.distributeCompartments(layerOutput, corenum, compartmentsPerCore)

        return layerOutput, corenum

    @classmethod
    def fullLayer(cls, layerInput, fullSpec, corenum, compartmentsPerCore, targetInput=None, targetConnProto=None):
        """Create a new fully connected layer.

        :param CompartmentGroup layerInput: The input to the fully connected layer
        :param dict fullSpec: Specifies "dim", the number of neurons,
                             "connProto", "compProto" prototypes of the layer,
                             "weightFile" where the weights can be read in from.
        :param int corenum: The last output of distributeCompartments()
        :param int compartmentsPerCore: The maximum number of compartments per core
        """
        # properties of the input layer
        nInput = layerInput.numNodes
        net = layerInput.net

        # properties of the convolution function
        compProto = fullSpec["compProto"]
        connProto = fullSpec["connProto"]
        dim = fullSpec["dim"]
        weightFile = fullSpec["weightFile"]
        nOutput = dim

        # temporary until we have files for weights of all layers
        if weightFile is not None:
            if weightFile[-3:] == 'npy':
                weight = np.load(weightFile) # pickled
            else:
                weight = np.loadtxt(weightFile).reshape(nOutput, nInput) # not pickled
        else:
            np.random.seed(0)
            # Let's try randomly initialized weights instead of zero
            # use 2 for 20 shot
            # use 20 for 1 and 5 (can also use 10 for 5)
            weight = np.random.randint(2, size=(nOutput, nInput))#np.ones((nOutput, nInput))*127
            #np.zeros((nOutput, nInput), dtype=int)
            
        print(weight.shape)
        print(weight)

        layerOutput = net.createCompartmentGroup(size=nOutput, prototype=compProto)

        conn = layerInput.connect(layerOutput, prototype=connProto, weight=weight)
        if targetInput is not None:
            targetInput.connect(layerOutput, prototype=targetConnProto, weight=127*np.eye(5, dtype='int'))

        corenum = cls.distributeCompartments(layerOutput, corenum, compartmentsPerCore)

        return layerOutput, corenum, conn
    
    @classmethod
    def fullLayerMC(cls, layerInput, fullSpec, corenum, compartmentsPerCore, targetInput=None, tg=None):
        """Create a new fully connected layer.

        :param CompartmentGroup layerInput: The input to the fully connected layer
        :param dict fullSpec: Specifies "dim", the number of neurons,
                             "connProto", "compProto" prototypes of the layer,
                             "weightFile" where the weights can be read in from.
        :param int corenum: The last output of distributeCompartments()
        :param int compartmentsPerCore: The maximum number of compartments per core
        """
        # properties of the input layer
        nInput = layerInput.numNodes
        net = layerInput.net

        # properties of the convolution function
        compProto = fullSpec["compProto"]
        connProtoDendrite = fullSpec["connProtoDendrite"]
        connProtoSoma = fullSpec["connProtoSoma"]
        dim = fullSpec["dim"]
        filename = fullSpec["weightFile"]
        nOutput = dim

        # temporary until we have files for weights of all layers
        if filename is not None:
            weight = np.loadtxt(filename).reshape(nOutput, nInput)
        else:
            np.random.seed(0)
            # Let's try randomly initialized weights instead of zero
            weight = np.random.randint(126, size=(nOutput, nInput))#np.ones((nOutput, nInput))*127
            #np.zeros((nOutput, nInput), dtype=int)
            
        print(weight.shape)
        print(weight)

        layerOutput = net.createNeuronGroup(size=nOutput, prototype=compProto)

        conn = layerInput.connect(layerOutput.dendrites[0], prototype=connProtoDendrite, weight=weight)
        #layerInput.connect(layerOutput.dendrites[0], prototype=connProtoDendrite, weight=weight)

        if targetInput is not None:
            targetInput.connect(layerOutput.dendrites[0], prototype=connProtoSoma, weight=-127*np.eye(nOutput, dtype='int')) # was negative!!!!!!!!!!!!!
            
            targetInput.connect(layerOutput.soma, prototype=connProtoSoma, weight=127*np.eye(nOutput, dtype='int')) # was 128!

        corenum = cls.distributeCompartments(layerOutput, corenum, compartmentsPerCore)

        return layerOutput, corenum, conn   



    @staticmethod
    def saveBoard(board, filename, counterIds, learningCores=None, postCoreIds=None, postCompIds=None, dummyProbes=None, connProbes=None):
        """
        Writes an N2Board to a file which can be reloaded later.
        
        :param N2Board board: The board to be written
        :param string filename: The name of the file to write (without extension)
        :param list-probes dummyProbes: The dummy probes used to setup spike counters
        """
        
        # Determine the information required to later load the board
        boardId = board.id
        numChips = len(board.n2Chips)
        numCoresPerChip = [None]*numChips
        numSynapsesPerCore = [None]*numChips
        for ii in range(numChips):
            numCoresPerChip[ii] = len(board.n2Chips[ii].n2Cores)
            numSynapsesPerCore[ii] = [None]*numCoresPerChip[ii]
            for jj in range(numCoresPerChip[ii]):
                numSynapsesPerCore[ii][jj] = board.n2Chips[ii].n2Cores[jj].synapses.numNodes

        traceProfile = None
        stdpProfile = None

        if learningCores is not None:
            traceProfile = dict()
            stdpProfile = dict()
            for lc in learningCores:
                chip = lc//128
                core = lc%128
                name = '{},{}'.format(chip,core)
                traceProfile[name] = []
                stdpProfile[name] = []
                #print(board.n2Chips[chip].n2Cores[core].stdpPostState.data)
                for word in board.n2Chips[chip].n2Cores[core].stdpPostState.data:
                    #print(word)
                    #print(word._traceProfile)
                    #print(word._stdpProfile)
                    traceProfile[name].append(word._traceProfile)
                    stdpProfile[name].append(word._stdpProfile)

        with open(tempDir+ '/'+filename + '.pkl', 'wb') as fname:
            pickle.dump([learningCores, traceProfile, stdpProfile, board.options, board.lmtOptions, counterIds, boardId, numChips, numCoresPerChip, numSynapsesPerCore, postCoreIds, postCompIds, dummyProbes, connProbes], fname)

            
        # Dump the NeuroCores
        board.dumpNeuroCores(tempDir+ '/'+filename + '.board')
    
    @staticmethod
    def initBoard(filename):
        """
        Initializes a board object from file with the correct number of chips/cores/synapses.
        
        :param string filename: The name of the file to load from (without extension)
        
        :return N2Board board: The board object
        :returns:
            - board (N2Board): The N2Board object
            - dummyProbes (list-probes): A list of probes used to setup spike counters
        """
        with open(tempDir+ '/'+ filename + '.pkl', 'rb') as fname:
            learningCores, traceProfile, stdpProfile, options, lmtOptions, counterIds, boardId, numChips, numCoresPerChip, numSynapsesPerCore, postCoreIds, postCompIds, dummyProbes = pickle.load(fname)
            
        board = N2Board(boardId, numChips, numCoresPerChip, numSynapsesPerCore, options=options, lmtOptions=lmtOptions)
        
        return board, counterIds, learningCores, traceProfile, stdpProfile, postCoreIds, postCompIds, dummyProbes
        
    @staticmethod
    def loadBoard(board, filename, learningCores=None, traceProfile=None, stdpProfile=None):
        """
        Loads neurocore state from file.
        
        :param N2Board board: The board object for which neurocore state should be loaded
        :param string filename: The name of the file to load from (without extension)
        """
        board.loadNeuroCores(tempDir+ '/'+filename + '.board')
        board.sync = False

        if learningCores is not None:
            for lc in learningCores:
                chip = lc//128
                core = lc%128
                name = '{},{}'.format(chip,core)
                for ii in range(len(traceProfile[name])):
                    # automatically extends stdpPostState if ii is out of range
                    board.n2Chips[chip].n2Cores[core].stdpPostState[ii]
                    # than assign the previous values
                    board.n2Chips[chip].n2Cores[core].stdpPostState.data[ii]._traceProfile = traceProfile[name][ii] 
                    board.n2Chips[chip].n2Cores[core].stdpPostState.data[ii]._stdpProfile = stdpProfile[name][ii]
                    
                    board.n2Chips[chip].n2Cores[core].stdpPostState.push(ii)

        
        
    @staticmethod
    def setupSpikeCounters(outputLayer):
        probeCond = SpikeProbeCondition(tStart=100000000)
        dummyProbes = outputLayer.probe(nx.ProbeParameter.SPIKE, probeCond)
        return dummyProbes
    
    @staticmethod
    def prepSpikeCounter(board, numSamples, corenum):
    
        # Infer which chip the output neurons lie on from the corenum
        chipId = int(corenum/128)
        
        # Get the spikes back from loihi
        runMgmtProcess = board.createProcess("runMgmt",
                                             includeDir= snipDirAbsolute,
                                             cFilePath = snipDirAbsolute + "/runmgmt.c",
                                             funcName = "run_mgmt",
                                             guardName = "do_run_mgmt",
                                             phase = "mgmt",
                                             lmtId=0,
                                             chipId=chipId)

        # Create a channel named spikeCntr to get the spikes count information from Lakemont
        spikeCntrChannel = board.createChannel(b'nxspkcntr', "packed", numSamples+2)

        # Connecting spikeCntr from runMgmtProcess to SuperHost which is receiving spike count in the channel
        spikeCntrChannel.connect(runMgmtProcess, None)
        
        return spikeCntrChannel
    
    
    def prepErrorTrigger(board, numSamples, corenum):
        # error triggered learning snip
        
        # Infer which chip the output neurons lie on from the corenum
        chipId = int(corenum/128)
        
        runMgmtProcessPreLrn = board.createProcess("runMgmt-ET",
                                             includeDir= snipDirAbsolute,
                                             cFilePath = snipDirAbsolute + "/runmgmt-ET.c",
                                             funcName = "run_mgmt_ET",
                                             guardName = "do_run_mgmt_ET",
                                             phase = "preLearnMgmt",
                                             lmtId=0,
                                             chipId=chipId)

                         
    @staticmethod
    def getResults(spikeCntrChannel, numSamples, numClasses, dummyProbes, saveResults=True):
        results = spikeCntrChannel.read(numSamples)
        results = np.array(results).reshape((numSamples,16)) #16 due to the "packed" data type
        results = results[:,:numClasses]  #remove the extras
        
        print(results.shape)
        
        counterIds = [prb.n2Probe.counterId-32 for prb in dummyProbes[0].probes]
        
        print(counterIds)
        
        counterIds = [counter for counter in counterIds if counter < numClasses]
        
        print(counterIds)

        results = results[:, counterIds] #counterIds] # for some reason the mc version keeps having 1 more than size
        

        if saveResults is True:
            np.savetxt(tempDir+'/spikesCounterOut.txt', results, fmt='%i')
        return results
    
    @staticmethod
    def checkAccuracy(labels, results, isFewShot):
        numSamples = results.shape[0]
        classification = [None]*numSamples
        numCorrect = 0
        # should get rid of hard coding
        breakDown = {1 : [], 
                     3 : [],
                     5 : [],
                     7 : [],
                     9 : []}
        for ii in range(numSamples):
            print("result",results[ii,:])
            maxLogicals = np.amax(results[ii,:])==results[ii,:]
            print("logical",maxLogicals)
            classification[ii] = np.where(maxLogicals)[0]
            if isFewShot:
                classification[ii] = [2*jj+1 for jj in classification[ii]]
                
            print("classification",classification[ii])
            print("label",labels[ii])

            if labels[ii] in classification[ii]:
                numCorrect += 1/len(classification[ii])
                if not breakDown[labels[ii]]:
                    # if empty put 1
                    breakDown[labels[ii]].append(1/len(classification[ii]))
                else:
                    breakDown[labels[ii]].append((sum(breakDown[labels[ii]]) + 1/len(classification[ii]))/(len(breakDown[labels[ii]])+1)) 
            else:
                if not breakDown[labels[ii]]:
                    # if empty put 1
                    breakDown[labels[ii]].append(0)
                else:
                    breakDown[labels[ii]].append((sum(breakDown[labels[ii]]))/(len(breakDown[labels[ii]])+1))
                
        accuracy = numCorrect/len(labels)
        return accuracy, breakDown
    
    
    def writeConstants(inputConnectionGroup, layerOutput, spikesPerPacket, sampleLength, postCoreIds, postCompIds, C, timestepspersample, num_shots, labelStart, learnList, blankTime, isi_div):
        constantsFile = snipDir + '/ET-Constants.h'
        f = open(constantsFile, "w")
        f.write('/* Temporary generated file for define the size of arrays before compilation */\n')
        f.write('#define timesteps_per_sample ' + str(timestepspersample)+'\n')
        f.write('#define div ' + str(isi_div)+'\n')
        f.write('#define num_classes ' + str(layerOutput.numNodes)+'\n')
        f.write('#define num_shots ' + str(num_shots)+'\n')
        f.write('#define SampleLength ' + str(sampleLength)+'\n')
        # stuff I added
        f.write('#define BlankTime ' + str(blankTime)+'\n')
        f.write('#define POST_CORE_IDS {' + ','.join(str(x) for x in postCoreIds) + '}\n')
        f.write('#define POST_COMP_IDS  {' + ','.join(str(x) for x in postCompIds) + '}\n')
        f.write('#define Y1 ' + str(C)+'\n')
        f.write('#define LABEL_TIMES {' + ','.join('{'+str(x).replace('[','').replace(']','')+'}' for x in labelStart) + '}\n')
        f.write('#define LEARN_TIMES {' + ','.join(str(x) for x in learnList) + '}\n')
        f.close()

    
 #"/home/kenneth/Documents/nxsdk/0.95/loihi-soel-gesture/src/snips"
snipDir = "../src/snips/"
snipDirAbsolute = "/homes/kennetms/loihi-soel-gesture/src/snips"
tempDir = "temp"
try:
    os.mkdir(tempDir)
except OSError as e:
    if e.errno != errno.EEXIST:
        raise e
