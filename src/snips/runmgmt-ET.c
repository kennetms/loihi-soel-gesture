/*
Handles Error Calculations and Writing to Neuron Post-traces for Learning.

Author: Kenneth Stewart
# Copyright : (c) Kenneth Stewart
# Licence : GPLv3
*/

#include "runmgmt-ET.h"

int e[num_classes] = {0}; // For keeping track of errors
int S[num_classes] = {0};

// For writing to post trace
PostTraceEntry post_values[num_classes] = {0};
int post_core_ids[num_classes] = POST_CORE_IDS;
int post_comp_ids[num_classes] = POST_COMP_IDS;
CoreId core; //nx_nth_coreid(learningCore);
NeuronCore *nc;

// For controlling when learning happens
int class = 0;
int prev_class = 0;
int first_class = 1;
int C = Y1;
int err = 0;
int labels[num_classes][num_shots] = LABEL_TIMES;
classFound = 0;
prev_step = -1;
sample_time_total = num_classes*num_shots*SampleLength;
int thresh = 0; //timesteps_per_sample/10;
int learn_counter = 0; // position in LEARN_LIST
int learn_list[num_classes*num_shots] = LEARN_TIMES;
int numClasses = num_classes;
int isi_div = div;

// Decides when learning should happen
int do_run_mgmt_ET(runState *s)
{

    if(s->time_step >= SampleLength*(learn_counter+1)-BlankTime && s->time_step < (SampleLength)*(learn_counter+1))
    {
         // Should not be learning during blank times when no sample is being shown
         core.id = post_core_ids[class];
         nc = NEURON_PTR(core);
         post_values[class].Yepoch0 = C; 
         nc->stdp_post_state[post_comp_ids[class]] = post_values[class];
         return 0;
    }
    
    classFound = 0;
        for(int i=0; i<numClasses; i++)
        {
             for(int j=0; j<num_shots; ++j)
             {
                 if(!classFound)
                 {
                     if(s->time_step >= labels[i][j] && labels[i][j] > prev_step)
                     {
                         class = i;
                         if(first_class)
                         {
                             first_class = 0;
                         }
                         else
                         {
                             learn_counter = learn_counter + 1;
                         }
                         classFound = 1;
                         prev_step = s->time_step;
                     }
                 }
             }
            // Ensure that neurons that shouldn't be learning are not learning
            if(i!=class)
            {
                 core.id = post_core_ids[i];
                 nc = NEURON_PTR(core);
                 post_values[i].Yepoch0 = C; 
                 nc->stdp_post_state[post_comp_ids[i]] = post_values[i];
                 S[i] = 0;
            }
        }  
    
    if(learn_list[learn_counter] == 0) // this is a time period for inference
    {
        core.id = post_core_ids[class];
         nc = NEURON_PTR(core);
         post_values[class].Yepoch0 = C; 
         nc->stdp_post_state[post_comp_ids[class]] = post_values[class];
        return 0;
    }
    
    return 1;
}

// Runs error-triggered learning
// Will only write to post trace when error is above or below the threshold
void run_mgmt_ET(runState *s){
    // this is the learning neuron
    core.id = post_core_ids[class];
    nc = NEURON_PTR(core);
    
    
    err = (timesteps_per_sample/isi_div - S[class]);
    if(err != 0 && ((err > thresh) || (err < -thresh)))
    {
        e[class] = ((C+err) >= 0) ? C+err : 0;
        S[class] = 0;
        thresh = thresh + 1;
    }
    else
    {
      // don't update
      e[class] = C;
        if (thresh > 0)
        {
          thresh = thresh - 1;
        }
    }
    post_values[class].Yepoch0 = e[class];
    nc->stdp_post_state[post_comp_ids[class]] = post_values[class];

}