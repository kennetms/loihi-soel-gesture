/*
Handles neuron spike counting for communicating with spike counters
The spike counts are used to determine the amount of error a neuron has with it's spiking.

Author: Kenneth Stewart
# Copyright : (c) Kenneth Stewart
# Licence : GPLv3
*/

#include <stdlib.h>
#include <string.h>
#include "runmgmt.h"
#include <time.h>
#include <unistd.h>

static int count = 0;
static int channelID = -1;
static int32_t spike_counts[16] = {0}; //assumes 16 or fewer output classes for now

int do_run_mgmt(runState *s) {
        
        if (s->time_step==1){
            channelID = getChannelID("nxspkcntr");
        }
    
        if(!testing)
        {
            return 1;
        }
        else if((count == timesteps_per_sample-1) || (count%512 == 0)) {
            return 1; //read the spike counters
        }else{
            count = count + 1;
            return 0; //otherwise do nothing this time
        }
}

void run_mgmt(runState *s) {

        // Read spike counters
        for(int probe_id = 0; probe_id<num_classes; probe_id++) {
                //------- to probe on every timestep, use this:
                spike_counts[probe_id] += SPIKE_COUNT[(s->time_step-1)&3][0x20+probe_id]; 
            
                // To keep track of spike counts for error measuring
                if(!testing)
                {
                    S[probe_id] += SPIKE_COUNT[(s->time_step-1)&3][0x20+probe_id];
                }
                
            
                SPIKE_COUNT[(s->time_step-1)&3][0x20+probe_id] = 0;
        }
    

        if (count == timesteps_per_sample-1) {
            count = 0;
            // Write the spike counter value back to the channel and reset our spike counts
            writeChannel(channelID, spike_counts, 1);
            for(int probe_id = 0; probe_id<num_classes; probe_id++)
                spike_counts[probe_id] = 0;
        }else{
            count = count + 1;
        }
}
