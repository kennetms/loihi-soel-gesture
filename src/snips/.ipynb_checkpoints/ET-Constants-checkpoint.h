/* Temporary File for communicating information between python files and snips

Author: Kenneth Stewart
# Copyright : (c) Kenneth Stewart
# Licence : GPLv3
*/

#define timesteps_per_sample 20
#define div 2
#define num_classes 5
#define num_shots 1
#define SampleLength 1540
#define BlankTime 100
#define POST_CORE_IDS {134,134,134,134,134}
#define POST_COMP_IDS  {0,1,2,3,4}
#define Y1 20
#define LABEL_TIMES {{0},{1540},{3080},{4620},{6160}}
#define LEARN_TIMES {1,1,1,1,1}
