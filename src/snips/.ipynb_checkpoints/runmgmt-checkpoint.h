/*
Handles neuron spike counting for communicating with spike counters
The spike counts are used to determine the amount of error a neuron has with it's spiking.

Author: Kenneth Stewart
# Copyright : (c) Kenneth Stewart
# Licence : GPLv3
*/

#include "array_sizes.h"
#include "nxsdk.h"

extern int S[];
extern int class;
extern int counter_ids[];

int do_run_mgmt(runState *s);
void run_mgmt(runState *s);

