/*
Handles Error Calculations and Writing to Neuron Post-traces for Learning.

Author: Kenneth Stewart
# Copyright : (c) Kenneth Stewart
# Licence : GPLv3
*/

#include "nxsdk.h"
#include "ET-Constants.h"

extern int S[];
extern int counter_ids[];

int do_run_mgmt_ET(runState *s);
void run_mgmt_ET(runState *s);

